﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EMS.Entities;
using Moq;
using EMS.Repository;
using Services;
using System.Collections.Generic;

namespace ServicesTest
{
    [TestClass]
    public class EventTests
    {
        [TestMethod]
        public void UpdateEvent_NonExisting()
        {
            Event newEv = new Event();
            Event addedEv = new Event() { IDEvent = 1 };

            Mock<IRepositoryFactory> repoFactory = new Mock<IRepositoryFactory>();
            repoFactory.Setup(obj => obj.GetRepository<IEventRepository>().Add(newEv)).Returns(addedEv);

            EventService service = new EventService(repoFactory.Object);

            Event ev = service.UpdateEvent(newEv);

            Assert.IsTrue(ev == addedEv);
        }

        [TestMethod]
        public void UpdateEvent_Existing()
        {
            Event existingEvent = new Event() { IDEvent = 1 };
            Event updatetdEvent = new Event() { IDEvent = 1 };

            Mock<IRepositoryFactory> repoFactory = new Mock<IRepositoryFactory>();
            repoFactory.Setup(obj => obj.GetRepository<IEventRepository>().Update(existingEvent)).Returns(updatetdEvent);

            EventService service = new EventService(repoFactory.Object);

            Event ev = service.UpdateEvent(existingEvent);

            Assert.IsTrue(ev == updatetdEvent);
        }

        [TestMethod]
        public void AddNewEvent()
        {
            Event newEv = new Event();
            Event addedEv = new Event() { IDEvent = 1 };

            Mock<IRepositoryFactory> repoFactory = new Mock<IRepositoryFactory>();
            repoFactory.Setup(obj => obj.GetRepository<IEventRepository>().Add(newEv)).Returns(addedEv);

            EventService service = new EventService(repoFactory.Object);

            Event ev = service.AddEvent(newEv);

            Assert.IsTrue(ev == addedEv);
        }

        [TestMethod]
        public void GetEvent()
        {
            Event newEv = new Event() { IDEvent = 1 };

            Mock<IRepositoryFactory> repoFactory = new Mock<IRepositoryFactory>();
            repoFactory.Setup(obj => obj.GetRepository<IEventRepository>().Get(1)).Returns(newEv);

            EventService service = new EventService(repoFactory.Object);

            Event ev = service.GetEvent(1);

            Assert.IsTrue(ev == newEv);
        }

        [TestMethod]
        public void GetEvenstByUser()
        {
            List<Event> events = new List<Event>()
            {
             new Event() { IDEvent = 1 },
             new Event() { IDEvent = 2 },
             new Event() { IDEvent = 3 }
            };

            Mock<IRepositoryFactory> repoFactory = new Mock<IRepositoryFactory>();
            repoFactory.Setup(obj => obj.GetRepository<IEventRepository>().GetEventsByUser(1)).Returns(events);

            EventService service = new EventService(repoFactory.Object);

            var ev = service.GetEventsByUser(1);

            Assert.AreEqual(ev, events);
        }

        [TestMethod]
        public void GetAllEvents()
        {
            List<Event> events = new List<Event>()
            {
             new Event() { IDEvent = 1 },
             new Event() { IDEvent = 2 },
             new Event() { IDEvent = 3 }
            };

            Mock<IRepositoryFactory> repoFactory = new Mock<IRepositoryFactory>();
            repoFactory.Setup(obj => obj.GetRepository<IEventRepository>().GetAll(1)).Returns(events);

            EventService service = new EventService(repoFactory.Object);

            var ev = service.GetAllEvents(1);

            Assert.AreEqual(ev, events);
        }



    }
}
