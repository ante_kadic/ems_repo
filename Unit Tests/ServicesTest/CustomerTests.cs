﻿using EMS.Entities;
using EMS.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicesTest
{
    [TestClass]
    public class CustomerTests
    {
        [TestMethod]
        public void UpdateCustomer_NonExisting()
        {
            Customer newCustomer = new Customer();
            Customer addedCustomer = new Customer() { IDCustomer = 1 };

            Mock<IRepositoryFactory> repoFactory = new Mock<IRepositoryFactory>();
            repoFactory.Setup(obj => obj.GetRepository<ICustomerRepository>().Add(newCustomer)).Returns(addedCustomer);

            CustomerService service = new CustomerService(repoFactory.Object);

            Customer ev = service.UpdateCustomer(newCustomer);

            Assert.IsTrue(ev == addedCustomer);
        }

        [TestMethod]
        public void UpdateCustomer_Existing()
        {
            Customer existingCustomer = new Customer() { IDCustomer = 1 };
            Customer updatetdCustomer = new Customer() { IDCustomer = 1 };

            Mock<IRepositoryFactory> repoFactory = new Mock<IRepositoryFactory>();
            repoFactory.Setup(obj => obj.GetRepository<ICustomerRepository>().Update(existingCustomer)).Returns(updatetdCustomer);

            CustomerService service = new CustomerService(repoFactory.Object);

            Customer ev = service.UpdateCustomer(existingCustomer);

            Assert.IsTrue(ev == updatetdCustomer);
        }

        [TestMethod]
        public void AddNewCustomer()
        {
            Customer newCustomer = new Customer();
            Customer addedCustomer = new Customer() { IDCustomer = 1 };

            Mock<IRepositoryFactory> repoFactory = new Mock<IRepositoryFactory>();
            repoFactory.Setup(obj => obj.GetRepository<ICustomerRepository>().Add(newCustomer)).Returns(addedCustomer);

            CustomerService service = new CustomerService(repoFactory.Object);

            Customer ev = service.AddCustomer(newCustomer);

            Assert.IsTrue(ev == addedCustomer);
        }

        [TestMethod]
        public void GetCustomer()
        {
            Customer newCustomer = new Customer() { IDCustomer = 1 };

            Mock<IRepositoryFactory> repoFactory = new Mock<IRepositoryFactory>();
            repoFactory.Setup(obj => obj.GetRepository<ICustomerRepository>().Get(1)).Returns(newCustomer);

            CustomerService service = new CustomerService(repoFactory.Object);

            Customer ev = service.GetCustomer(1);

            Assert.IsTrue(ev == newCustomer);
        }

        [TestMethod]
        public void GetAllCustomers()
        {
            List<Customer> Customers = new List<Customer>()
            {
             new Customer() { IDCustomer = 1 },
             new Customer() { IDCustomer = 2 },
             new Customer() { IDCustomer = 3 }
            };

            Mock<IRepositoryFactory> repoFactory = new Mock<IRepositoryFactory>();
            repoFactory.Setup(obj => obj.GetRepository<ICustomerRepository>().GetAll(1)).Returns(Customers);

            CustomerService service = new CustomerService(repoFactory.Object);

            var ev = service.GetAllCustomers(1);

            Assert.AreEqual(ev, Customers);
        }
    }
}
