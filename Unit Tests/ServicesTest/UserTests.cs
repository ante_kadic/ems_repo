﻿using EMS.Entities;
using EMS.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicesTest
{
    [TestClass]
    public class UserTests
    {
        [TestMethod]
        public void UpdateUser_NonExisting()
        {
            User newUser = new User();
            User addedUser = new User() { IDUser = 1 };

            Mock<IRepositoryFactory> repoFactory = new Mock<IRepositoryFactory>();
            repoFactory.Setup(obj => obj.GetRepository<IUserRepository>().Add(newUser)).Returns(addedUser);

            UserService service = new UserService(repoFactory.Object);

            User ev = service.UpdateUser(newUser);

            Assert.IsTrue(ev == addedUser);
        }

        [TestMethod]
        public void UpdateUser_Existing()
        {
            User existingUser = new User() { IDUser = 1 };
            User updatetdUser = new User() { IDUser = 1 };

            Mock<IRepositoryFactory> repoFactory = new Mock<IRepositoryFactory>();
            repoFactory.Setup(obj => obj.GetRepository<IUserRepository>().Update(existingUser)).Returns(updatetdUser);

            UserService service = new UserService(repoFactory.Object);

            User ev = service.UpdateUser(existingUser);

            Assert.IsTrue(ev == updatetdUser);
        }

        [TestMethod]
        public void AddNewUser()
        {
            User newUser = new User();
            User addedUser = new User() { IDUser = 1 };

            Mock<IRepositoryFactory> repoFactory = new Mock<IRepositoryFactory>();
            repoFactory.Setup(obj => obj.GetRepository<IUserRepository>().Add(newUser)).Returns(addedUser);

            UserService service = new UserService(repoFactory.Object);

            User ev = service.AddUser(newUser);

            Assert.IsTrue(ev == addedUser);
        }

        [TestMethod]
        public void GetUser()
        {
            User newUser = new User() { IDUser = 1 };

            Mock<IRepositoryFactory> repoFactory = new Mock<IRepositoryFactory>();
            repoFactory.Setup(obj => obj.GetRepository<IUserRepository>().Get(1)).Returns(newUser);

            UserService service = new UserService(repoFactory.Object);

            User ev = service.GetUser(1);

            Assert.IsTrue(ev == newUser);
        }

        [TestMethod]
        public void GetAllUsersByVendor()
        {
            List<User> Users = new List<User>()
            {
             new User() { IDUser = 1 },
             new User() { IDUser = 2 },
             new User() { IDUser = 3 }
            };

            Mock<IRepositoryFactory> repoFactory = new Mock<IRepositoryFactory>();
            repoFactory.Setup(obj => obj.GetRepository<IUserRepository>().GetAll(1)).Returns(Users);

            UserService service = new UserService(repoFactory.Object);

            var ev = service.GetAllUsers(1);

            Assert.AreEqual(ev, Users);
        }

        [TestMethod]
        public void ValidateUser()
        {
            string hash = PasswordHash.CreateHash("test");
            string hash1 = PasswordHash.CreateHash("test");
            string hash2 = PasswordHash.CreateHash("test");

            User pero = new User() { IDUser = 1, UserName = "Pero", PasswordHash = hash };
            User josip = new User() { IDUser = 2, UserName = "Josip", PasswordHash = hash1 };
            User marko = new User() { IDUser = 3, UserName = "Marko", PasswordHash = hash2 };


            List<User> users = new List<User>()
            {
                pero,josip,marko
            };

            Mock<IRepositoryFactory> repoFactory = new Mock<IRepositoryFactory>();
            repoFactory.Setup(obj => obj.GetRepository<IUserRepository>().GetAll()).Returns(users);

            UserService service = new UserService(repoFactory.Object);

            var ev = service.ValidateUser("Pero", "test");

            Assert.AreEqual(ev, pero);
            Assert.AreNotEqual(ev, josip);
            Assert.AreNotEqual(ev, marko);
        }

        [TestMethod]
        public void ValidateUser_wrongUsername()
        {
            string hash = PasswordHash.CreateHash("test");
            string hash1 = PasswordHash.CreateHash("test");
            string hash2 = PasswordHash.CreateHash("test");

            User pero = new User() { IDUser = 1, UserName = "Pero", PasswordHash = hash };
            User josip = new User() { IDUser = 2, UserName = "Josip", PasswordHash = hash1 };
            User marko = new User() { IDUser = 3, UserName = "Marko", PasswordHash = hash2 };


            List<User> users = new List<User>()
            {
                pero,josip,marko
            };

            Mock<IRepositoryFactory> repoFactory = new Mock<IRepositoryFactory>();
            repoFactory.Setup(obj => obj.GetRepository<IUserRepository>().GetAll()).Returns(users);

            UserService service = new UserService(repoFactory.Object);

            var ev = service.ValidateUser("Per", "test");

            Assert.IsNull(ev);
        }

        [TestMethod]
        public void ValidateUser_wrongPassword()
        {
            string hash = PasswordHash.CreateHash("test");
            string hash1 = PasswordHash.CreateHash("test");
            string hash2 = PasswordHash.CreateHash("test");

            User pero = new User() { IDUser = 1, UserName = "Pero", PasswordHash = hash };
            User josip = new User() { IDUser = 2, UserName = "Josip", PasswordHash = hash1 };
            User marko = new User() { IDUser = 3, UserName = "Marko", PasswordHash = hash2 };


            List<User> users = new List<User>()
            {
                pero,josip,marko
            };

            Mock<IRepositoryFactory> repoFactory = new Mock<IRepositoryFactory>();
            repoFactory.Setup(obj => obj.GetRepository<IUserRepository>().GetAll()).Returns(users);

            UserService service = new UserService(repoFactory.Object);

            var ev = service.ValidateUser("Pero", "teste");

            Assert.IsNull(ev);
        }
    }
}
