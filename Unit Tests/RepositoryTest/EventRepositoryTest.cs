﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EMS.Repository;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.Composition;
using EMS.Entities;
using Moq;

namespace RepositoryTest
{
    [TestClass]
    public class EventRepositoryTest
    {
        [Import]
        IRepositoryFactory _repositoryFactory;
        IEventRepository _repository;

        [TestInitialize]
        public void Initialize()
        {
            RepositoryContainerLoader.Container.SatisfyImportsOnce(this);
            _repository = _repositoryFactory.GetRepository<IEventRepository>();
        }

        [TestMethod]
        public void GetAllEvents_FromDatabase()
        {
            var events = _repository.GetAll(1);

            Assert.IsTrue(events != null);
        }

        [TestMethod]
        public void GetAllEvents_Mock()
        {
            int vendorId = 1;

            List<Event> events = new List<Event>()
            {
                new Event(){ EventDate = DateTime.Now.AddMonths(3),Deposit = 300 },
                new Event(){ EventDate = DateTime.Now.AddMonths(4),Deposit = 500 },
                new Event(){ EventDate = DateTime.Now.AddMonths(5),Deposit = 600 },
                new Event(){ EventDate = DateTime.Now.AddMonths(6),Deposit = 200 },
            };

            Mock<IEventRepository> mockRepository = new Mock<IEventRepository>();
            mockRepository.Setup(obj => obj.GetAll(vendorId)).Returns(events);

            var entities = mockRepository.Object.GetAll(vendorId);

            Assert.AreEqual(events, entities);

        }

        public void GetEvent_Mock()
        {
            var ev = new Event() { IDEvent = 1, EventDate = DateTime.Now.AddMonths(3), Deposit = 300 };

            Mock<IEventRepository> mockRepository = new Mock<IEventRepository>();
            mockRepository.Setup(obj => obj.Get(1)).Returns(ev);

            var ent = mockRepository.Object.Get(1);

            Assert.AreEqual(ev, ent);

        }

        [TestMethod]
        public void EventsByUser()
        {
            int userId = 1;
            var ev1 = new Event() { EventDate = DateTime.Now.AddMonths(3), Deposit = 300, VendorID = 2 };
            var ev2 = new Event() { EventDate = DateTime.Now.AddMonths(4), Deposit = 500, VendorID = 2 };
            var ev3 = new Event() { EventDate = DateTime.Now.AddMonths(5), Deposit = 600, VendorID = 2 };
            var ev4 = new Event() { EventDate = DateTime.Now.AddMonths(6), Deposit = 200, VendorID = 2 };

            List<Event> events = new List<Event>()
            {
                ev1,ev2,ev3,ev4
            };

            Mock<IEventRepository> mockRepository = new Mock<IEventRepository>();
            mockRepository.Setup(obj => obj.GetEventsByUser(1)).Returns(events);

            var ent = mockRepository.Object.GetEventsByUser(1);

            Assert.AreEqual(events, ent);

        }

        [TestMethod]
        public void EventsByUserWithNegativeID()
        {
            var events = _repository.GetEventsByUser(-2);

            Assert.AreEqual(events.Count(), 0);
        }

        [TestMethod]
        public void EventsByUserWithZeroVendorID()
        {
            var events = _repository.GetEventsByUser(1);

            Assert.AreEqual(events.Count(), 0);
        }

    }
}
