﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Common
{
    public static class Extensions
    {
        public static string GetTwoDigits(this int num)
        {
            return num > 9 ? num.ToString() : "0" + num;
        }

        public static string GetThreeDigits(this int num)
        {
            if (num < 10)
                return "00" + num;

            else if (num < 100)
                return "0" + num;

            return num.ToString();
        }
    }
}
