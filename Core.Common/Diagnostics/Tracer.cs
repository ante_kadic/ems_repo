﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Common
{
    public enum TraceType
    {
        Console, File, Full
    }

    public enum TraceLevel
    {
        Warning, Information, Exception
    }

    public class Tracer
    {
        static string FILE_PATH_FORMAT = "Logs\\{0}-{1}-{2}-{3}.txt";
        static string EXCEPTION_FORMAT = "{0}:{1}:{2}:{3} (EXC): {4}    InnerExc: {5} {6}";
        static string INFO_FORMAT = "{0}:{1}:{2}:{3} (Inf): {4}";
        static string WARNING_FORMAT = "{0}:{1}:{2}:{3} (WAR): {4}";

        public static void Trace(Exception ex, TraceLevel traceLevel = TraceLevel.Exception, TraceType type = TraceType.Full)
        {
            var test = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Production" && traceLevel != TraceLevel.Exception)
                return;

            string format = traceLevel == TraceLevel.Exception ? EXCEPTION_FORMAT : WARNING_FORMAT;

            string message = string.Format(format,
                DateTime.Now.Hour.GetTwoDigits(),
                DateTime.Now.Minute.GetTwoDigits(),
                DateTime.Now.Second.GetTwoDigits(),
                DateTime.Now.Millisecond.GetThreeDigits(),
                    ex.Message, ex.InnerException?.Message, ex.StackTrace);

            if (type == TraceType.File || type == TraceType.Full)
                WriteToFile(message);

            if (type == TraceType.Console || type == TraceType.Full)
                WriteToConsole(message, traceLevel == TraceLevel.Exception ? ConsoleColor.Red : ConsoleColor.Yellow);

        }

        public static void Trace(string message, TraceLevel traceLevel = TraceLevel.Information, TraceType type = TraceType.Full)
        {
            if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Production" && traceLevel != TraceLevel.Exception)
                return;

            string format = traceLevel == TraceLevel.Information ? INFO_FORMAT : WARNING_FORMAT;

            string info = string.Format(format,
                DateTime.Now.Hour.GetTwoDigits(),
                DateTime.Now.Minute.GetTwoDigits(),
                DateTime.Now.Second.GetTwoDigits(),
                DateTime.Now.Millisecond.GetThreeDigits(),
                    message);

            if (type == TraceType.File || type == TraceType.Full)
                WriteToFile(info);

            if (type == TraceType.Console || type == TraceType.Full)
                WriteToConsole(info, traceLevel == TraceLevel.Information ? ConsoleColor.White : ConsoleColor.Yellow);
        }

        private static void WriteToFile(string message)
        {
            var path = string.Format
                (FILE_PATH_FORMAT, DateTime.Now.Year,
                DateTime.Now.Month.GetTwoDigits(),
                DateTime.Now.Day.GetTwoDigits(),
                System.AppDomain.CurrentDomain.FriendlyName.Remove(System.AppDomain.CurrentDomain.FriendlyName.Length - 12));

            new FileInfo(path).Directory.Create();

            using (var sw = new StreamWriter(path, true))
            {
                sw.WriteLine(message);
            }
        }

        private static void WriteToConsole(string message, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
