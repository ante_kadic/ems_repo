﻿using Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceComponentContainer
{
    public static class ServiceContainerLoader
    {
        private static CompositionContainer _container;

        public static CompositionContainer Container
        {
            get
            {
                if (_container == null)
                    _container = Init();

                return _container;
            }
        }

        private static CompositionContainer Init()
        {
            AggregateCatalog catalog = new AggregateCatalog();

            catalog.Catalogs.Add(new AssemblyCatalog(typeof(EventService).Assembly));
            catalog.Catalogs.Add(new AssemblyCatalog(typeof(UserService).Assembly));
            catalog.Catalogs.Add(new AssemblyCatalog(typeof(CustomerService).Assembly));

            return new CompositionContainer(catalog);
        }
    }
}
