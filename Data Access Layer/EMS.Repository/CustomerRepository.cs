﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using EMS.Entities;
using System.ComponentModel.Composition;

namespace EMS.Repository
{
    [Export(typeof(ICustomerRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CustomerRepository : BaseRepository, ICustomerRepository
    {
        public Customer Add(Customer entity)
        {
            using (var context = CreateContext())
            {
                var sameNameCustomers = context.Customers.Where(x =>
                        x.VendorID == entity.VendorID &&
                        x.FirstName == entity.FirstName &&
                        x.LastName == entity.LastName);

                if (sameNameCustomers.Any())
                {
                    Customer existingCustomer;
                    if (string.IsNullOrEmpty(entity.PersonalID))
                        existingCustomer = sameNameCustomers.Where(x =>
                            x.PhoneNumber == entity.PhoneNumber ||
                            x.Email == entity.Email)
                            .FirstOrDefault();

                    else
                    {
                        existingCustomer = sameNameCustomers.Where(x =>
                                x.PersonalID == entity.PersonalID)
                                .FirstOrDefault();

                        if (existingCustomer == null)
                            existingCustomer = sameNameCustomers.Where(x =>
                                x.PhoneNumber == entity.PhoneNumber ||
                                x.Email == entity.Email)
                                .FirstOrDefault();
                    }

                    if (existingCustomer != null)
                        return existingCustomer;
                }

                var savedCustomer = context.Customers.Add(entity);
                context.SaveChanges();

                return savedCustomer;
            }
        }

        public Customer Get(int id)
        {
            using (var context = CreateContext())
            {
                return context.Customers
                    .Where(x => x.IDCustomer == id)
                    .Include(x => x.EventCustomers.Select(y => y.Event))
                    .Include(x => x.Vendor)
                    .FirstOrDefault();
            }
        }

        public IEnumerable<Customer> GetAll(int vendorId)
        {
            using (var context = CreateContext())
            {
                return context.Customers
                    .Where(x => x.VendorID == vendorId)
                    .Include(x => x.EventCustomers.Select(y => y.Event))
                    .Include(x => x.Vendor)
                    .ToList();
            }
        }

        public Customer Update(Customer entity)
        {
            if (entity == null)
                throw new ArgumentNullException();

            using (var context = CreateContext())
            {
                Customer customerToUpdate = context.GetCustomer(entity.IDCustomer).First();

                context.Entry(customerToUpdate).CurrentValues.SetValues(entity);

                context.SaveChanges();
                return entity;
            }
        }

        public void Remove(int id)
        {
            throw new NotImplementedException();
        }
    }
}
