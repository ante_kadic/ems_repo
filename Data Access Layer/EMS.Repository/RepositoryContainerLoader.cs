﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Repository
{
    public static class RepositoryContainerLoader
    {
        private static CompositionContainer _container;

        public static CompositionContainer Container
        {
            get
            {
                if (_container == null)
                    _container = Init();

                return _container;
            }
        }

        private static CompositionContainer Init()
        {
            AggregateCatalog catalog = new AggregateCatalog();

            catalog.Catalogs.Add(new AssemblyCatalog(typeof(EventRepository).Assembly));

            return new CompositionContainer(catalog);
        }
    }
}
