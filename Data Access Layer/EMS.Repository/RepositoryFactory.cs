﻿using System.ComponentModel.Composition;


namespace EMS.Repository
{
    [Export(typeof(IRepositoryFactory))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class RepositoryFactory : IRepositoryFactory
    {
        public T GetRepository<T>() where T : IRepository
        {
            return RepositoryContainerLoader.Container.GetExportedValue<T>();
        }

    }
}
