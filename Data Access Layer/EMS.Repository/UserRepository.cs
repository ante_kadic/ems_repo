﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Data.Entity;
using EMS.Entities;

namespace EMS.Repository
{
    [Export(typeof(IUserRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class UserRepository : BaseRepository, IUserRepository
    {
        public User Add(User entity)
        {
            using (var context = CreateContext())
            {
                var savedUser = context.Users.Add(entity);
                context.SaveChanges();

                return savedUser;
            }
        }

        public User Get(int id)
        {
            using (var context = CreateContext())
            {
                return context.Users
                    .FirstOrDefault(x => x.IDUser == id);
            }
        }

        public IEnumerable<User> GetAll(int vendorId)
        {
            using (var context = CreateContext())
            {
                return context.Users
                     .Where(x => x.VendorID == vendorId)
                    .Include(x => x.EventUsers.Select(y => y.Event))
                    .Include(x => x.Vendor)
                    .ToList();
            }
        }

        public IEnumerable<User> GetAll()
        {
            using (var context = CreateContext())
            {
                return context.Users.ToList();
            }
        }

        public User Get(string userName, string password)
        {
            using (var context = CreateContext())
            {
                return context.Users
                     .Where(x => x.UserName == userName && x.PasswordHash == password)
                    .Include(x => x.EventUsers.Select(y => y.Event))
                    .Include(x => x.Vendor)
                    .FirstOrDefault();
            }
        }

        public User Update(User entity)
        {
            if (entity == null)
                throw new ArgumentNullException();

            using (var context = CreateContext())
            {

                User userToUpdate = context.GetUser(entity.IDUser).First();

                userToUpdate.Address = entity.Address;
                userToUpdate.BirthDate = entity.BirthDate;
                userToUpdate.Email = entity.Email;
                userToUpdate.FirstName = entity.FirstName;
                userToUpdate.LastName = entity.LastName;
                userToUpdate.IsAdmin = entity.IsAdmin;
                userToUpdate.PhoneNumber = entity.PhoneNumber;
                userToUpdate.UserName = entity.UserName;

                context.Users.Attach(userToUpdate);

                context.Entry(userToUpdate).Property(x => x.Address).IsModified = true;
                context.Entry(userToUpdate).Property(x => x.BirthDate).IsModified = true;
                context.Entry(userToUpdate).Property(x => x.Email).IsModified = true;
                context.Entry(userToUpdate).Property(x => x.FirstName).IsModified = true;
                context.Entry(userToUpdate).Property(x => x.LastName).IsModified = true;
                context.Entry(userToUpdate).Property(x => x.IsAdmin).IsModified = true;
                context.Entry(userToUpdate).Property(x => x.PhoneNumber).IsModified = true;
                context.Entry(userToUpdate).Property(x => x.UserName).IsModified = true;

                context.SaveChanges();
                return entity;
            }
        }

        public void UpdateUserImage(int id, byte[] picture)
        {
            using (var context = CreateContext())
            {
                User userToUpdate = context.GetUser(id).First();
                userToUpdate.Picture = picture;

                context.Users.Attach(userToUpdate);
                context.Entry(userToUpdate).Property(x => x.Picture).IsModified = true;
                context.SaveChanges();
            }
        }

        public void AssignUserToEvent(int eventId, int userId)
        {
            using (var context = CreateContext())
            {
                EventUser eu = new EventUser()
                {
                    EventID = eventId,
                    UserID = userId
                };

                context.EventUsers.Add(eu);
                context.SaveChanges();
            }
        }

        public void UpdateUserPassword(int id, string newPasswordHash)
        {
            using (var context = CreateContext())
            {
                User userToUpdate = context.GetUser(id).First();
                userToUpdate.PasswordHash = newPasswordHash;

                context.Users.Attach(userToUpdate);
                context.Entry(userToUpdate).Property(x => x.PasswordHash).IsModified = true;
                context.SaveChanges();
            }
        }

        public IEnumerable<PlannerEntry> GetPlannerEntries(int userId)
        {
            using (var context = CreateContext())
            {
                return context.PlannerEntries
                    .Where(x => x.UserId == userId)
                    .ToList();
            }
        }

        public PlannerEntry UpdatePlannerEntry(PlannerEntry entry)
        {
            using (var context = CreateContext())
            {
                var entryToUpdate = context.PlannerEntries.FirstOrDefault(x => x.ID == entry.ID);

                context.Entry(entryToUpdate).CurrentValues.SetValues(entry);

                context.SaveChanges();
                return entry;
            }
        }

        public PlannerEntry AddPlannerEntry(PlannerEntry entry)
        {
            using (var context = CreateContext())
            {
                PlannerEntry saved = context.PlannerEntries.Add(entry);

                context.SaveChanges();
                return saved;
            }
        }

        public void Remove(int id)
        {
            throw new NotImplementedException();
        }
    }
}
