﻿using EMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Repository
{
    public interface IEventRepository : IRepository<Event>
    {
        IEnumerable<Event> GetEventsByUser(int userId);

        IEnumerable<WeddingHall> GetWeddingHalls(int vendorId);

        IEnumerable<WeddingHall> GetWeddingHallsEvents(int vendorId);

        WeddingHall GetWeddingHall(int id);

        void UpdateEventDeposit(int eventId, decimal deposit, int currency);

        void UpdateEventState(int eventId, int newState);

        void UpdatePeopleNumber(int eventId, int peopleNo);

        void UpdateEventMenu(int eventId, int menuId);

        IEnumerable<Menu> GetMenus(int vendorId);

        Menu AddMenu(Menu menu);

        Menu UpdateMenu(Menu menu);

        WeddingHall AddWeddingHall(WeddingHall weddingHall);

        WeddingHall UpdateWeddingHall(WeddingHall weddingHall);

    }
}
