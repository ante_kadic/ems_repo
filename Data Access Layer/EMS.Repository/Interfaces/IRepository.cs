﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Repository
{
    public interface IRepository
    {

    }

    public interface IRepository<T> : IRepository
    {
        IEnumerable<T> GetAll(int vendorId);

        T Get(int id);

        T Update(T entity);

        void Remove(int id);

        T Add(T entity);
    }
}
