﻿using EMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Repository
{
    public interface ICustomerRepository : IRepository<Customer>
    {

    }
}
