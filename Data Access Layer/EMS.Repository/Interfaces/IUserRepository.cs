﻿using EMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Repository
{
    public interface IUserRepository : IRepository<User>
    {
        IEnumerable<User> GetAll();

        void UpdateUserImage(int id, byte[] picture);

        void UpdateUserPassword(int id, string newPasswordHash);

        void AssignUserToEvent(int eventId, int userId);

        IEnumerable<PlannerEntry> GetPlannerEntries(int userId);

        PlannerEntry UpdatePlannerEntry(PlannerEntry entry);

        PlannerEntry AddPlannerEntry(PlannerEntry entry);
        
    }
}
