﻿using EMS.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Repository
{
    [Export(typeof(IEventRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class EventRepository : BaseRepository, IEventRepository
    {
        public IEnumerable<Event> GetAll(int vendorId)
        {
            using (var context = CreateContext())
            {
                return context.GetAllEvents(vendorId).ToList();
            }
        }

        public Event Get(int id)
        {
            using (var context = CreateContext())
            {
                return context.Events
                    .Where(x => x.IDEvent == id)
                    .Include(x => x.EventCustomers.Select(y => y.Customer))
                    .Include(x => x.EventUsers.Select(y => y.User))
                    .Include(x => x.Menu)
                    .Include(x => x.WeddingHall)
                    .FirstOrDefault();
            }
        }

        public Event Add(Event ev)
        {
            using (var context = CreateContext())
            {
                var savedEvent = context.Events.Add(ev);
                context.SaveChanges();

                return savedEvent;
            }
        }

        public void Remove(int id)
        {
            throw new NotImplementedException();
        }

        public Event Update(Event ev)
        {
            if (ev == null)
                throw new ArgumentNullException();

            using (var context = CreateContext())
            {
                var eventToUpdate = context.GetEvent(ev.IDEvent).First();

                context.Entry(eventToUpdate).CurrentValues.SetValues(ev);

                context.SaveChanges();
                return ev;
            }
        }

        public IEnumerable<WeddingHall> GetWeddingHalls(int vendorId)
        {
            using (var context = CreateContext())
            {
                return context.GetWeddingHalls(vendorId).ToList();
            }
        }

        public IEnumerable<WeddingHall> GetWeddingHallsEvents(int vendorId)
        {
            using (var context = CreateContext())
            {
                var hallEvents = context.WeddingHalls
                    .Where(x => x.VendorID == vendorId)
                    .Include(x => x.Events.Select(y => y.EventUsers.Select(eu => eu.User)))
                    .ToList();

                var finStatus = context.EventStatus.FirstOrDefault(x => x.Description == "Finished");

                var eventsNeedToUpdate = hallEvents
                    .SelectMany(x => x.Events)
                    .Where(y => y.EventDate < DateTime.Now.AddDays(-1) &&
                    y.StatusID != finStatus.ID);

                if (eventsNeedToUpdate.Any())
                {
                    foreach (var ev in eventsNeedToUpdate)
                    {
                        ev.StatusID = finStatus.ID;
                    }
                    context.SaveChanges();
                }

                return hallEvents;
            }
        }

        public WeddingHall GetWeddingHall(int id)
        {
            using (var context = CreateContext())
            {
                return context.WeddingHalls
                    .Where(x => x.IDWeddingHall == id)
                    .Include(x => x.Events.Select(y => y.EventUsers.Select(eu => eu.User)))
                    .FirstOrDefault();
            }
        }

        public IEnumerable<Event> GetEventsByUser(int userId)
        {
            using (var context = CreateContext())
            {
                return context.GetEventsByUser(userId).ToList();
            }
        }

        public void UpdateEventDeposit(int eventId, decimal deposit, int currency)
        {
            using (var context = CreateContext())
            {
                var eventToUpdate = context.GetEvent(eventId).First();

                eventToUpdate.Deposit = deposit;
                eventToUpdate.DepositCurrency = currency;

                context.Events.Attach(eventToUpdate);

                context.Entry(eventToUpdate).Property(x => x.Deposit).IsModified = true;
                context.Entry(eventToUpdate).Property(x => x.DepositCurrency).IsModified = true;

                context.SaveChanges();
            }
        }

        public void UpdateEventState(int eventId, int newState)
        {
            using (var context = CreateContext())
            {
                var eventToUpdate = context.GetEvent(eventId).First();

                eventToUpdate.StatusID = newState;

                context.Events.Attach(eventToUpdate);

                context.Entry(eventToUpdate).Property(x => x.StatusID).IsModified = true;

                context.SaveChanges();
            }
        }

        public void UpdatePeopleNumber(int eventId, int peopleNo)
        {
            using (var context = CreateContext())
            {
                var eventToUpdate = context.GetEvent(eventId).First();

                eventToUpdate.PeopleCapacity = peopleNo;

                context.Events.Attach(eventToUpdate);

                context.Entry(eventToUpdate).Property(x => x.PeopleCapacity).IsModified = true;

                context.SaveChanges();
            }
        }

        public void UpdateEventMenu(int eventId, int menuId)
        {
            using (var context = CreateContext())
            {
                var eventToUpdate = context.GetEvent(eventId).First();

                eventToUpdate.MenuID = menuId;

                context.Events.Attach(eventToUpdate);

                context.Entry(eventToUpdate).Property(x => x.MenuID).IsModified = true;

                context.SaveChanges();
            }
        }

        public IEnumerable<Menu> GetMenus(int vendorId)
        {
            using (var context = CreateContext())
            {
                return context.Menus.Where(x => x.VendorID == vendorId).ToList();
            }
        }

        public Menu AddMenu(Menu menu)
        {
            if (menu == null)
                throw new ArgumentNullException();

            using (var context = CreateContext())
            {
                var savedMenu = context.Menus.Add(menu);

                context.SaveChanges();
                return savedMenu;
            }
        }

        public Menu UpdateMenu(Menu menu)
        {
            if (menu == null)
                throw new ArgumentNullException();

            using (var context = CreateContext())
            {
                var menuToUpdate = context.Menus.FirstOrDefault(x => x.IDMenu == menu.IDMenu);

                context.Entry(menuToUpdate).CurrentValues.SetValues(menu);

                context.SaveChanges();
                return menu;
            }
        }

        public WeddingHall AddWeddingHall(WeddingHall weddingHall)
        {
            if (weddingHall == null)
                throw new ArgumentNullException();

            using (var context = CreateContext())
            {
                var savedHall = context.WeddingHalls.Add(weddingHall);

                context.SaveChanges();
                return savedHall;
            }
        }

        public WeddingHall UpdateWeddingHall(WeddingHall weddingHall)
        {
            if (weddingHall == null)
                throw new ArgumentNullException();

            using (var context = CreateContext())
            {
                var hallToUpdate = context.WeddingHalls.FirstOrDefault(x => x.IDWeddingHall == weddingHall.IDWeddingHall);

                context.Entry(hallToUpdate).CurrentValues.SetValues(weddingHall);

                context.SaveChanges();
                return weddingHall;
            }
        }
    }
}
