﻿using Core.Common;
using Engine.Gateway;
using Engine.Gateway.CM_ServiceReference;
using Engine.Gateway.EM_ServiceReference;
using Engine.Gateway.UM_ServiceReference;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace EngineHost.Console
{
    class Program
    {
        private static ProcessManager _processManger;

        static void Main(string[] args)
        {
            //Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "Production");
            GatewayEngine engine = new GatewayEngine();

            RunServiceProccesses();

            using (var host = new ServiceHost(engine))
            {
                host.Closing += Host_Closing;
                host.Closed += Host_Closed;

                Tracer.Trace("------------------->  Gateway starting  <---------------------", type: TraceType.Console);
                System.Console.WriteLine();

                host.Open();

                Tracer.Trace("Service started");
                Tracer.Trace("", type: TraceType.Console);

                foreach (var endpoint in host.Description.Endpoints)
                {
                    Tracer.Trace("Listening on endpoint:", type: TraceType.Console);
                    Tracer.Trace($"\tAddress: {endpoint.Address.Uri.ToString()}", type: TraceType.Console);
                    Tracer.Trace($"\tBinding: {endpoint.Binding.Name}", type: TraceType.Console);
                    Tracer.Trace($"\tContract: {endpoint.Contract.ConfigurationName}", type: TraceType.Console);
                }

                System.Console.WriteLine("");
                System.Console.ReadLine();
            }
        }

        private static void Host_Closed(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private static void Host_Closing(object sender, EventArgs e)
        {
            _processManger.KillProccesses();
        }

        private static void RunServiceProccesses()
        {
            _processManger = new ProcessManager();

            _processManger.RunProccess<IEventService>();
            _processManger.RunProccess<IUserService>();
            _processManger.RunProccess<ICustomerService>();
        }
    }
}
