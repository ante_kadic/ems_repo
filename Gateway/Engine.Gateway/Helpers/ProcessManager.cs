﻿using Core.Common;
using Engine.Gateway.CM_ServiceReference;
using Engine.Gateway.EM_ServiceReference;
using Engine.Gateway.UM_ServiceReference;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Gateway
{
    public class ProcessManager
    {
        List<Process> processes;

        public ProcessManager()
        {
            processes = new List<Process>();
        }

        public bool RunProccess<T>()
        {
            if (typeof(T) == typeof(IUserService))
            {
                try
                {
                    Process userProcess = Process.Start("UMServiceHost.Console.exe");
                    processes.Add(userProcess);
                    return true;
                }
                catch (Exception ex)
                {
                    Tracer.Trace(ex);
                    return false;
                }
            }
            else if (typeof(T) == typeof(IEventService))
            {
                try
                {
                    Process userProcess = Process.Start("EMServiceHost.Console.exe");
                    processes.Add(userProcess);
                    return true;
                }
                catch (Exception ex)
                {
                    Tracer.Trace(ex);
                    return false;
                }
            }
            else if (typeof(T) == typeof(ICustomerService))
            {
                try
                {
                    Process userProcess = Process.Start("CMServiceHost.Console.exe");
                    processes.Add(userProcess);
                    return true;
                }
                catch (Exception ex)
                {
                    Tracer.Trace(ex);
                    return false;
                }
            }

            throw new NotImplementedException();
        }

        public void KillProccesses()
        {
            foreach (var p in processes)
            {
                p.Kill();
            }
        }
    }
}
