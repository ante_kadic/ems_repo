﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Gateway
{
    [DataContract]
    public class VendorDTO
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int IDVendor { get; set; }
    }
}
