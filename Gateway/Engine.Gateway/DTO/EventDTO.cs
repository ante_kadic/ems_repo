﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Gateway
{
    [DataContract]
    public class EventDTO
    {
        public EventDTO()
        {
            Users = new List<UserDTO>();
            Customers = new List<CustomerDTO>();
        }

        [DataMember]
        public int IDEvent { get; set; }
        [DataMember]
        public int TypeID { get; set; }
        [DataMember]
        public int StatusID { get; set; }
        [DataMember]
        public int HallID { get; set; }
        [DataMember]
        public string HallName { get; set; }
        [DataMember]
        public Nullable<int> MenuID { get; set; }
        [DataMember]
        public string MenuName { get; set; }
        [DataMember]
        public decimal MenuPrice { get; set; }
        [DataMember]
        public Nullable<System.DateTime> DateArranged { get; set; }
        [DataMember]
        public Nullable<System.DateTime> EventDate { get; set; }
        [DataMember]
        public Nullable<System.DateTime> DepositDueDate { get; set; }
        [DataMember]
        public Nullable<System.DateTime> CapacityDueDate { get; set; }
        [DataMember]
        public decimal Deposit { get; set; }
        [DataMember]
        public int DepositCurrency { get; set; }
        [DataMember]
        public int PeopleCapacity { get; set; }
        [DataMember]
        public int VendorID { get; set; }
        [DataMember]
        public MenuDTO Menu { get; set; }
        [DataMember]
        public List<UserDTO> Users { get; set; }
        [DataMember]
        public List<CustomerDTO> Customers { get; set; }

    }
}
