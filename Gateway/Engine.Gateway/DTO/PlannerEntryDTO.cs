﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Gateway
{
    [DataContract]
    public class PlannerEntryDTO
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public System.DateTime Date { get; set; }

        [DataMember]
        public int UserId { get; set; }
    }
}
