﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Gateway
{
    [DataContract]
    public class UserImageDTO
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Picture { get; set; }
    }
}
