﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Gateway
{
    [DataContract]
    public class WeddingHallDTO
    {
        [DataMember]
        public int IDWeddingHall { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public Nullable<int> Min_Capacity { get; set; }
        [DataMember]
        public Nullable<int> Max_Capacity { get; set; }
        [DataMember]
        public int VendorID { get; set; }
        [DataMember]
        public List<EventDTO> Events { get; set; }
    }
}
