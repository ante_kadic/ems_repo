﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Gateway
{
    [DataContract]
    public class MenuDTO
    {
        [DataMember]
        public int IDMenu { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public int VendorID { get; set; }
        [DataMember]
        public int CurrencyID { get; set; }
    }
}
