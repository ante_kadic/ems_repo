﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Gateway
{
    [DataContract]
    public class EventCustomerDTO
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public int EventID { get; set; }
        [DataMember]
        public int CustomerID { get; set; }
    }
}
