﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Gateway
{
    [DataContract]
    public class CustomerDTO
    {

        [DataMember]
        public int IDCustomer { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string PhoneNumber { get; set; }
        [DataMember]
        public string PersonalID { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public int VendorID { get; set; }
        [DataMember]
        public int EventID { get; set; }
    }
}
