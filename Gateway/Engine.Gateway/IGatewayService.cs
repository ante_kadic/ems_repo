﻿using Core.Common;
using EMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Engine.Gateway
{
    [ServiceContract]
    public interface IGatewayService
    {
        #region Events

        [OperationContract]
        EventDTO GetEvent(string id);

        [OperationContract]
        IEnumerable<EventDTO> GetAllEvents(string vendorId);

        [OperationContract]
        IEnumerable<EventDTO> GetEventsByUser(string userId);

        [OperationContract]
        EventDTO UpdateEvent(EventDTO ev);

        [OperationContract]
        EventDTO AddEvent(EventDTO ev);

        [OperationContract]
        void DeleteEvent(string id);

        [OperationContract]
        IEnumerable<WeddingHallDTO> GetWeddingHalls(string vendorId);

        [OperationContract]
        IEnumerable<WeddingHallDTO> GetWeddingHallsEvents(string vendorId);

        [OperationContract]
        WeddingHallDTO GetWeddingHall(string id);

        [OperationContract]
        void UpdateEventDeposit(string eventId, string deposit, string currency);

        [OperationContract]
        void UpdateEventState(string eventId, string newState);

        [OperationContract]
        void UpdatePeopleNumber(string eventId, string peopleNo);

        [OperationContract]
        void UpdateEventMenu(string eventId, string menuId);

        [OperationContract]
        IEnumerable<MenuDTO> GetMenus(string vendorId);

        [OperationContract]
        MenuDTO UpdateMenu(MenuDTO menu);

        [OperationContract]
        MenuDTO AddMenu(MenuDTO menu);

        [OperationContract]
        WeddingHallDTO UpdateWeddingHall(WeddingHallDTO weddingHall);

        [OperationContract]
        WeddingHallDTO AddWeddingHall(WeddingHallDTO weddingHall);
        #endregion

        #region Users

        [OperationContract]
        UserDTO GetUser(string id);

        [OperationContract]
        IEnumerable<UserDTO> GetAllUsers(string vendorId);

        [OperationContract]
        UserDTO UpdateUser(UserDTO user);

        [OperationContract]
        void UpdateUserImage(UserImageDTO userImage);

        [OperationContract]
        UserDTO AddUser(UserDTO user);

        [OperationContract]
        void DeleteUser(string id);

        [OperationContract]
        UserDTO ValidateUser(string userName, string password);

        [OperationContract]
        void UpdateUserPassword(string id, string password);

        [OperationContract]
        void AssignUserToEvent(string eventId, string userId);

        [OperationContract]
        IEnumerable<PlannerEntryDTO> GetPlannerEntries(string userId);

        [OperationContract]
        PlannerEntryDTO UpdatePlannerEntry(PlannerEntryDTO entry);

        [OperationContract]
        PlannerEntryDTO AddPlannerEntry(PlannerEntryDTO entry);

        #endregion

        #region Customers

        [OperationContract]
        CustomerDTO GetCustomer(string id);

        [OperationContract]
        IEnumerable<CustomerDTO> GetAllCustomers(string vendorId);

        [OperationContract]
        CustomerDTO UpdateCustomer(CustomerDTO customer);

        [OperationContract]
        CustomerDTO AddCustomer(CustomerDTO customer);

        #endregion
    }
}
