﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using EMS.Entities;
using Engine.Gateway.EM_ServiceReference;
using Core.Common;
using Engine.Gateway.UM_ServiceReference;
using Engine.Gateway.CM_ServiceReference;
using Services;
using System.ServiceModel.Web;
using System.Runtime.Serialization.Json;
using System.IO;
using AutoMapper;
using System.Web.Script.Serialization;
using System.Runtime.Caching;

namespace Engine.Gateway
{

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Single)]
    public class GatewayEngine : ServiceBase, IGatewayService
    {

        #region Fields

        IMapper mapper;
        IEventService _eventService;
        IUserService _userService;
        ICustomerService _customerService;

        #endregion

        #region Constructor

        public GatewayEngine()
        {
            InitMapperConfiguration();
            InitServices();
        }

        #endregion

        #region Init

        private void InitMapperConfiguration()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Customer, CustomerDTO>();
                cfg.CreateMap<CustomerDTO, Customer>();
                cfg.CreateMap<Event, EventDTO>();
                cfg.CreateMap<EventDTO, Event>();
                cfg.CreateMap<EventCustomer, EventCustomerDTO>();
                cfg.CreateMap<EventCustomerDTO, EventCustomer>();
                cfg.CreateMap<EventStatu, EventStatusDTO>();
                cfg.CreateMap<EventStatusDTO, EventStatu>();
                cfg.CreateMap<EventType, EventTypeDTO>();
                cfg.CreateMap<EventTypeDTO, EventType>();
                cfg.CreateMap<EventUser, EventUserDTO>();
                cfg.CreateMap<EventUserDTO, EventUser>();
                cfg.CreateMap<Menu, MenuDTO>();
                cfg.CreateMap<MenuDTO, Menu>();
                cfg.CreateMap<User, UserDTO>();
                cfg.CreateMap<UserDTO, User>();
                cfg.CreateMap<Vendor, VendorDTO>();
                cfg.CreateMap<VendorDTO, Vendor>();
                cfg.CreateMap<WeddingHall, WeddingHallDTO>();
                cfg.CreateMap<WeddingHallDTO, WeddingHall>();
                cfg.CreateMap<PlannerEntry, PlannerEntryDTO>();
                cfg.CreateMap<PlannerEntryDTO, PlannerEntry>();
            });

            mapper = config.CreateMapper();
        }

        private void InitServices()
        {
            CreateChannels();
        }

        private void CreateChannels()
        {
            ChannelFactory<IEventService> cf = new ChannelFactory<IEventService>("BasicHttpBinding_IEventService");
            ChannelFactory<IUserService> ucf = new ChannelFactory<IUserService>("BasicHttpBinding_IUserService");
            ChannelFactory<ICustomerService> ccf = new ChannelFactory<ICustomerService>("BasicHttpBinding_ICustomerService");

            _eventService = cf.CreateChannel();
            _userService = ucf.CreateChannel();
            _customerService = ccf.CreateChannel();
        }

        #endregion

        #region Event

        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "event/{id}")]
        public EventDTO GetEvent(string id)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                Event ev = _eventService.GetEvent(int.Parse(id));

                var eventDto = mapper.Map<Event, EventDTO>(ev);
                var customers = mapper.Map<List<Customer>, List<CustomerDTO>>(ev.EventCustomers.Select(x => x.Customer).ToList());
                var users = mapper.Map<List<User>, List<UserDTO>>(ev.EventUsers.Select(x => x.User).ToList());

                eventDto.HallName = ev.WeddingHall.Name;
                eventDto.Customers.AddRange(customers);
                eventDto.Users.AddRange(users);

                if (ev.Menu != null)
                {
                    eventDto.MenuName = ev.Menu.Name;
                    eventDto.MenuPrice = ev.Menu.Price;
                }

                return eventDto;
            });
        }

        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "events/{vendorId}")]
        public IEnumerable<EventDTO> GetAllEvents(string vendorId)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                Event[] events = _eventService.GetAllEvents(int.Parse(vendorId));

                return mapper.Map<Event[], EventDTO[]>(events);
            });
        }

        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "eventsByUser/{userId}")]
        public IEnumerable<EventDTO> GetEventsByUser(string userId)
        {
            return ExecuteFaultHandledOperation(() =>
           {
               Event[] events = _eventService.GetEventsByUser(int.Parse(userId));

               return mapper.Map<Event[], EventDTO[]>(events);
           });
        }

        [WebInvoke(
            Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "event")]
        public EventDTO AddEvent(EventDTO ev)
        {
            return ExecuteFaultHandledOperation(() =>
           {
               List<CustomerDTO> savedCustomers = new List<CustomerDTO>();

               Event even = mapper.Map<EventDTO, Event>(ev);
               foreach (var user in ev.Users)
                   even.EventUsers.Add(new EventUser() { EventID = even.IDEvent, UserID = user.IDUser });

               foreach (var customer in ev.Customers)
               {
                   customer.VendorID = ev.VendorID;
                   var savedCustomer = _customerService.AddCustomer(mapper.Map<CustomerDTO, Customer>(customer));

                   even.EventCustomers.Add(new EventCustomer() { EventID = even.IDEvent, CustomerID = savedCustomer.IDCustomer });

                   savedCustomers.Add(mapper.Map<Customer, CustomerDTO>(savedCustomer));
               }

               Event savedEvent = _eventService.AddEvent(even);

               var eventDTO = mapper.Map<Event, EventDTO>(savedEvent);

               foreach (var sc in savedCustomers)
                   eventDTO.Customers.Add(sc);

               return eventDTO;
           });
        }

        [WebInvoke(
            Method = "PUT",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "event")]
        public EventDTO UpdateEvent(EventDTO ev)
        {
            return ExecuteFaultHandledOperation(() =>
           {
               Event even = _eventService.UpdateEvent(mapper.Map<EventDTO, Event>(ev));

               return mapper.Map<Event, EventDTO>(even);
           });
        }

        [WebInvoke(
            Method = "PUT",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "event/{eventId}/{deposit}/{currency}")]
        public void UpdateEventDeposit(string eventId, string deposit, string currency)
        {
            ExecuteFaultHandledOperation(() =>
            {
                _eventService.UpdateEventDeposit(int.Parse(eventId), decimal.Parse(deposit), int.Parse(currency));
            });
        }

        [WebInvoke(
            Method = "PUT",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "event/{eventId}/{newState}")]
        public void UpdateEventState(string eventId, string newState)
        {
            ExecuteFaultHandledOperation(() =>
            {
                _eventService.UpdateEventState(int.Parse(eventId), int.Parse(newState));
            });
        }

        [WebInvoke(
            Method = "PUT",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "event/people/{eventId}/{peopleNo}")]
        public void UpdatePeopleNumber(string eventId, string peopleNo)
        {
            ExecuteFaultHandledOperation(() =>
            {
                _eventService.UpdatePeopleNumber(int.Parse(eventId), int.Parse(peopleNo));
            });
        }

        [WebInvoke(
            Method = "DELETE",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "event/{id}")]
        public void DeleteEvent(string id)
        {
            ExecuteFaultHandledOperation(() =>
           {
               _eventService.DeleteEvent(int.Parse(id));
           });
        }

        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "halls/{vendorId}")]
        public IEnumerable<WeddingHallDTO> GetWeddingHalls(string vendorId)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                var halls = _eventService.GetWeddingHalls(int.Parse(vendorId));
                return mapper.Map<WeddingHall[], WeddingHallDTO[]>(halls);

            });
        }

        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "halls/deepload/{vendorId}")]
        public IEnumerable<WeddingHallDTO> GetWeddingHallsEvents(string vendorId)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                WeddingHall[] halls = _eventService.GetWeddingHallsEvents(int.Parse(vendorId));

                var hallsDto = mapper.Map<WeddingHall[], WeddingHallDTO[]>(halls);

                foreach (var hall in halls)
                {
                    var h = hallsDto.FirstOrDefault(x => x.IDWeddingHall == hall.IDWeddingHall);
                    foreach (var ev in hall.Events)
                    {
                        var e = h.Events.FirstOrDefault(y => y.IDEvent == ev.IDEvent);
                        var usersDto = mapper.Map<User[], UserDTO[]>(ev.EventUsers.Select(u => u.User).ToArray());
                        e.Users.AddRange(usersDto);
                        e.HallName = hall.Name;
                    }
                }
                return hallsDto;
            });
        }

        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "hall/{id}")]
        public WeddingHallDTO GetWeddingHall(string id)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                var hall = _eventService.GetWeddingHall(int.Parse(id));

                return mapper.Map<WeddingHall, WeddingHallDTO>(hall);

            });
        }

        [WebInvoke(
            Method = "PUT",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "menu/{eventID}/{menuId}")]
        public void UpdateEventMenu(string eventId, string menuId)
        {
            ExecuteFaultHandledOperation(() =>
            {
                _eventService.UpdateEventMenu(int.Parse(eventId), int.Parse(menuId));
            });
        }

        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "menu/{vendorId}")]
        public IEnumerable<MenuDTO> GetMenus(string vendorId)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                var menus = _eventService.GetMenus(int.Parse(vendorId));

                return mapper.Map<Menu[], MenuDTO[]>(menus);

            });
        }

        [WebInvoke(
            Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "hall")]
        public WeddingHallDTO AddWeddingHall(WeddingHallDTO weddingHall)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                WeddingHall saved = _eventService.AddWeddingHall(mapper.Map<WeddingHallDTO, WeddingHall>(weddingHall));

                return mapper.Map<WeddingHall, WeddingHallDTO>(saved);
            });
        }

        [WebInvoke(
            Method = "PUT",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "hall")]
        public WeddingHallDTO UpdateWeddingHall(WeddingHallDTO weddingHall)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                WeddingHall saved = _eventService.UpdateWeddingHall(mapper.Map<WeddingHallDTO, WeddingHall>(weddingHall));

                return mapper.Map<WeddingHall, WeddingHallDTO>(saved);
            });
        }

        [WebInvoke(
            Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "menu")]
        public MenuDTO AddMenu(MenuDTO menu)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                Menu saved = _eventService.AddMenu(mapper.Map<MenuDTO, Menu>(menu));

                return mapper.Map<Menu, MenuDTO>(saved);
            });
        }

        [WebInvoke(
            Method = "PUT",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "menu")]
        public MenuDTO UpdateMenu(MenuDTO menu)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                Menu saved = _eventService.UpdateMenu(mapper.Map<MenuDTO, Menu>(menu));

                return mapper.Map<Menu, MenuDTO>(saved);
            });
        }

        #endregion

        #region User

        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "users/{vendorId}")]
        public IEnumerable<UserDTO> GetAllUsers(string vendorId)
        {
            return ExecuteFaultHandledOperation(() =>
           {
               User[] users = _userService.GetAllUsers(int.Parse(vendorId));

               return mapper.Map<User[], UserDTO[]>(users);
           });
        }

        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "user/{id}")]
        public UserDTO GetUser(string id)
        {
            return ExecuteFaultHandledOperation(() =>
           {
               User user = _userService.GetUser(int.Parse(id));

               return mapper.Map<User, UserDTO>(user);
           });
        }

        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            UriTemplate = "user")]
        public UserDTO AddUser(UserDTO user)
        {
            return ExecuteFaultHandledOperation(() =>
           {
               User us = _userService.AddUser(mapper.Map<UserDTO, User>(user));

               return mapper.Map<User, UserDTO>(us);
           });
        }

        [WebInvoke(
            Method = "PUT",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "user")]
        public UserDTO UpdateUser(UserDTO user)
        {
            return ExecuteFaultHandledOperation(() =>
           {
               User us = _userService.UpdateUser(mapper.Map<UserDTO, User>(user));

               return mapper.Map<User, UserDTO>(us);
           });
        }

        [WebInvoke(
            Method = "PUT",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "user/image")]
        public void UpdateUserImage(UserImageDTO userImage)
        {
            ExecuteFaultHandledOperation(() =>
            {
                _userService.UpdateUserImage(userImage.ID, userImage.Picture);
            });
        }

        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "user/{eventId}/{userId}")]
        public void AssignUserToEvent(string eventId, string userId)
        {
            ExecuteFaultHandledOperation(() =>
            {
                _userService.AssignUserToEvent(int.Parse(eventId), int.Parse(userId));
            });
        }

        [WebInvoke(
            Method = "PUT",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "user/password/{id}/{password}")]
        public void UpdateUserPassword(string id, string password)
        {
            ExecuteFaultHandledOperation(() =>
            {
                _userService.UpdateUserPassword(int.Parse(id), password);
            });
        }

        [WebInvoke(
            Method = "DELETE",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "user/{id}")]
        public void DeleteUser(string id)
        {
            ExecuteFaultHandledOperation(() =>
           {
               _userService.DeleteUser(int.Parse(id));
           });
        }

        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "user/{userName}/{password}")]
        public UserDTO ValidateUser(string userName, string password)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                User user = _userService.ValidateUser(userName, password);

                return mapper.Map<User, UserDTO>(user);
            });
        }

        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "entries/{userId}")]
        public IEnumerable<PlannerEntryDTO> GetPlannerEntries(string userId)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                PlannerEntry[] entries = _userService.GetPlannerEntries(int.Parse(userId));

                return mapper.Map<PlannerEntry[], PlannerEntryDTO[]>(entries);
            });
        }

        [WebInvoke(
            Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "entry")]
        public PlannerEntryDTO AddPlannerEntry(PlannerEntryDTO entry)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                PlannerEntry savedEntry = _userService.AddPlannerEntry(mapper.Map<PlannerEntryDTO, PlannerEntry>(entry));

                return mapper.Map<PlannerEntry, PlannerEntryDTO>(savedEntry);
            });
        }

        [WebInvoke(
            Method = "PUT",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "entry")]
        public PlannerEntryDTO UpdatePlannerEntry(PlannerEntryDTO entry)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                PlannerEntry savedEntry = _userService.UpdatePlannerEntry(mapper.Map<PlannerEntryDTO, PlannerEntry>(entry));

                return mapper.Map<PlannerEntry, PlannerEntryDTO>(savedEntry);
            });
        }


        #endregion

        #region Customer

        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "customer/{id}")]
        public CustomerDTO GetCustomer(string id)
        {
            return ExecuteFaultHandledOperation(() =>
           {
               Customer customer = _customerService.GetCustomer(int.Parse(id));

               return mapper.Map<Customer, CustomerDTO>(customer);
           });
        }

        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "customers/{vendorId}")]
        public IEnumerable<CustomerDTO> GetAllCustomers(string vendorId)
        {
            return ExecuteFaultHandledOperation(() =>
           {
               Customer[] customers = _customerService.GetAllCustomers(int.Parse(vendorId));

               return mapper.Map<Customer[], CustomerDTO[]>(customers);
           });
        }

        [WebInvoke(
            Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "customer")]
        public CustomerDTO AddCustomer(CustomerDTO customer)
        {
            return ExecuteFaultHandledOperation(() =>
           {
               Customer c = mapper.Map<CustomerDTO, Customer>(customer);
               c.EventCustomers.Add(new EventCustomer() { CustomerID = customer.IDCustomer, EventID = customer.EventID });

               Customer savedCustomer = _customerService.AddCustomer(c);

               return mapper.Map<Customer, CustomerDTO>(savedCustomer);
           });
        }

        [WebInvoke(
            Method = "PUT",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "customer")]
        public CustomerDTO UpdateCustomer(CustomerDTO customer)
        {
            return ExecuteFaultHandledOperation(() =>
           {
               Customer cust = _customerService.UpdateCustomer(mapper.Map<CustomerDTO, Customer>(customer));

               return mapper.Map<Customer, CustomerDTO>(cust);
           });
        }

        #endregion

    }
}
