﻿using Core.Common;
using EMS.Entities;
using Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Services
{
    [ServiceContract]
    public interface ICustomerService : IDataService
    {
        [OperationContract]
        Customer GetCustomer(int id);

        [OperationContract]
        IEnumerable<Customer> GetAllCustomers(int vendorId);

        [OperationContract]
        Customer UpdateCustomer(Customer ev);

        [OperationContract]
        Customer AddCustomer(Customer ev);

    }
    
}
