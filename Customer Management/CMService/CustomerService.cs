﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using EMS.Entities;
using EMS.Repository;
using System.ComponentModel.Composition;
using Core.Common;

namespace Services
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    public class CustomerService : ServiceBase, ICustomerService
    {
        [Import]
        IRepositoryFactory _repositoryFactory;

        public CustomerService()
        {
        }

        public CustomerService(IRepositoryFactory repositoryFactory)
        {
            _repositoryFactory = repositoryFactory;
        }

        public Customer AddCustomer(Customer customer)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                Customer addedCustomer;

                if (customer == null)
                {
                    ArgumentNullException ex = new ArgumentNullException("null Customer provided to add in database");
                    throw new FaultException<ArgumentNullException>(ex, ex.Message);
                }

                ICustomerRepository customerRepository = _repositoryFactory.GetRepository<ICustomerRepository>();
                addedCustomer = customerRepository.Add(customer);
                if (addedCustomer == null)
                {
                    NullReferenceException ex = new NullReferenceException(string.Format($"Customer with ID of {customer.IDCustomer} returned NULL after trying to add in database"));
                    throw new FaultException<NullReferenceException>(ex, ex.Message);
                }
                return addedCustomer;
            });

        }

        public IEnumerable<Customer> GetAllCustomers(int vendorId)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                ICustomerRepository customerRepository = _repositoryFactory.GetRepository<ICustomerRepository>();
                return customerRepository.GetAll(vendorId);
            });
        }

        public Customer GetCustomer(int id)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                ICustomerRepository customerRepository = _repositoryFactory.GetRepository<ICustomerRepository>();

                Customer customer = customerRepository.Get(id);
                if (customer == null)
                {
                    NotFoundException ex = new NotFoundException(string.Format($"Customer with id:{id} not found in database"));
                    throw new FaultException<NotFoundException>(ex, ex.Message);
                }
                return customer;
            });
        }

        public Customer UpdateCustomer(Customer customer)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                Customer updatedCustomer;
                if (customer == null)
                {
                    ArgumentNullException ex = new ArgumentNullException("null Customer provided for update");
                    throw new FaultException<ArgumentNullException>(ex, ex.Message);
                }

                ICustomerRepository customerRepository = _repositoryFactory.GetRepository<ICustomerRepository>();

                if (customer.IDCustomer == 0)
                    updatedCustomer = customerRepository.Add(customer);
                else
                    updatedCustomer = customerRepository.Update(customer);

                if (updatedCustomer == null)
                {
                    NullReferenceException ex = new NullReferenceException(string.Format($"Customer with ID of {customer.IDCustomer} returned NULL after trying to update in database"));
                    throw new FaultException<NullReferenceException>(ex, ex.Message);
                }
                return updatedCustomer;
            });

        }
    }
}
