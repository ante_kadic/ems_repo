﻿using Core.Common;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CMServiceHost.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            //Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "Production");

            using (var host = new ServiceHost(typeof(CustomerService)))
            {
                Tracer.Trace("------------------->  Customer Management starting  <---------------------");
                System.Console.WriteLine();

                host.Open();

                Tracer.Trace("Service started");
                Tracer.Trace("", type: TraceType.Console);

                foreach (var endpoint in host.Description.Endpoints)
                {
                    Tracer.Trace("Listening on endpoint:", type: TraceType.Console);
                    Tracer.Trace($"\tAddress: {endpoint.Address.Uri.ToString()}", type: TraceType.Console);
                    Tracer.Trace($"\tBinding: {endpoint.Binding.Name}", type: TraceType.Console);
                    Tracer.Trace($"\tContract: {endpoint.Contract.ConfigurationName}", type: TraceType.Console);
                }

                System.Console.WriteLine("");
                System.Console.ReadLine();
            }
        }

    }
}
