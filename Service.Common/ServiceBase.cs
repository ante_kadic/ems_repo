﻿using Core.Common;
using EMS.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class ServiceBase
    {
        private string MethodName
        {
            get
            {
                StackTrace st = new StackTrace();
                StackFrame sf = st.GetFrame(2);

                MethodBase currentMethodName = sf.GetMethod();
                return currentMethodName.Name;
            }
        }

        public ServiceBase()
        {
            RepositoryContainerLoader.Container.SatisfyImportsOnce(this);
        }

        protected T ExecuteFaultHandledOperation<T>(Func<T> codeToExecute)
        {
            try
            {
                Tracer.Trace($"Executing {MethodName}....");
                T result = codeToExecute.Invoke();

                Tracer.Trace($"Succesfully executed {MethodName}");

                return result;
            }
            catch (FaultException ex)
            {
                if (ex.Message.EndsWith("not found in database"))
                    Tracer.Trace(ex, Core.Common.TraceLevel.Warning);
                else
                    Tracer.Trace(ex);

                throw;
            }
            catch (Exception ex)
            {
                Tracer.Trace(ex);
                throw new FaultException(ex.Message);
            }
        }

        protected void ExecuteFaultHandledOperation(Action codeToExecute)
        {
            try
            {
                Tracer.Trace($"Executing method {MethodName}....");
                codeToExecute.Invoke();

                Tracer.Trace($"Succesfully executed {MethodName}");
            }
            catch (FaultException ex)
            {
                Tracer.Trace(ex);
                throw;
            }
            catch (Exception ex)
            {
                Tracer.Trace(ex);
                throw new FaultException(ex.Message);
            }
        }

    }
}
