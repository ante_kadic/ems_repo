﻿using Core.Common;
using EMS.Entities;
using EMS.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Services
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    public class UserService : ServiceBase, IUserService
    {
        [Import]
        IRepositoryFactory _repositoryFactory;


        public UserService()
        {
        }

        public UserService(IRepositoryFactory repositoryFactory)
        {
            _repositoryFactory = repositoryFactory;
        }

        public User GetUser(int id)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                IUserRepository userRepository = _repositoryFactory.GetRepository<IUserRepository>();

                User user = userRepository.Get(id);
                if (user == null)
                {
                    NotFoundException ex = new NotFoundException(string.Format($"Event with id:{id} not found in database"));
                    throw new FaultException<NotFoundException>(ex, ex.Message);
                }
                return user;
            });
        }

        public IEnumerable<User> GetAllUsers(int vendorId)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                IUserRepository userRepository = _repositoryFactory.GetRepository<IUserRepository>();
                return userRepository.GetAll(vendorId);
            });
        }

        public User UpdateUser(User user)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                User updatedUser;

                if (user == null)
                {
                    ArgumentNullException ex = new ArgumentNullException("null User provided for update");
                    throw new FaultException<ArgumentNullException>(ex, ex.Message);
                }
                IUserRepository userRepository = _repositoryFactory.GetRepository<IUserRepository>();

                if (user.IDUser == 0)
                {
                    user.PasswordHash = PasswordHash.CreateHash(user.PasswordHash);
                    updatedUser = userRepository.Add(user);
                }
                else
                    updatedUser = userRepository.Update(user);

                if (updatedUser == null)
                {
                    NullReferenceException ex = new NullReferenceException(string.Format($"User with ID of {user.IDUser} returned NULL after trying to update in database"));
                    throw new FaultException<NullReferenceException>(ex, ex.Message);
                }
                return updatedUser;
            });
        }

        public void UpdateUserImage(int id, string picture)
        {
            ExecuteFaultHandledOperation(() =>
            {
                IUserRepository userRepository = _repositoryFactory.GetRepository<IUserRepository>();
                userRepository.UpdateUserImage(id, Convert.FromBase64String(picture));
            });
        }

        public void UpdateUserPassword(int id, string newPassword)
        {
            ExecuteFaultHandledOperation(() =>
            {
                IUserRepository userRepository = _repositoryFactory.GetRepository<IUserRepository>();

                string newPasswordHash = PasswordHash.CreateHash(newPassword);
                userRepository.UpdateUserPassword(id, newPasswordHash);
            });
        }

        public User AddUser(User user)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                User addedUser;
                IUserRepository userRepository = _repositoryFactory.GetRepository<IUserRepository>();

                if (user == null)
                {
                    ArgumentNullException ex = new ArgumentNullException("null User provided to add in database");
                    throw new FaultException<ArgumentNullException>(ex, ex.Message);
                }

                user.PasswordHash = PasswordHash.CreateHash(user.PasswordHash);
                addedUser = userRepository.Add(user);
                if (addedUser == null)
                {
                    NullReferenceException ex = new NullReferenceException(string.Format($"User with ID of {user.IDUser} returned NULL after trying to add in database"));
                    throw new FaultException<NullReferenceException>(ex, ex.Message);
                }
                return addedUser;
            });
        }

        public void AssignUserToEvent(int eventId, int userId)
        {
            ExecuteFaultHandledOperation(() =>
            {
                IUserRepository userRepository = _repositoryFactory.GetRepository<IUserRepository>();
                userRepository.AssignUserToEvent(eventId, userId);
            });
        }

        public User ValidateUser(string userName, string password)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                IUserRepository userRepository = _repositoryFactory.GetRepository<IUserRepository>();
                IEnumerable<User> users = userRepository.GetAll();

                return users.FirstOrDefault(x =>
                    PasswordHash.ValidatePassword(password, x.PasswordHash)
                    && x.UserName == userName);
            });
        }

        public IEnumerable<PlannerEntry> GetPlannerEntries(int userId)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                IUserRepository userRepository = _repositoryFactory.GetRepository<IUserRepository>();
                return userRepository.GetPlannerEntries(userId);
            });
        }

        public PlannerEntry UpdatePlannerEntry(PlannerEntry entry)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                IUserRepository userRepository = _repositoryFactory.GetRepository<IUserRepository>();
                return userRepository.UpdatePlannerEntry(entry);
            });
        }

        public PlannerEntry AddPlannerEntry(PlannerEntry entry)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                IUserRepository userRepository = _repositoryFactory.GetRepository<IUserRepository>();
                return userRepository.AddPlannerEntry(entry);
            });
        }

        public void DeleteUser(int id)
        {
            throw new NotImplementedException();
        }
    }
}
