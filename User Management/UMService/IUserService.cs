﻿using Core.Common;
using EMS.Entities;
using Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Services
{
    [ServiceContract]
    public interface IUserService : IDataService
    {
        [OperationContract]
        User GetUser(int id);

        [OperationContract]
        IEnumerable<User> GetAllUsers(int vendorId);

        [OperationContract]
        User UpdateUser(User ev);

        [OperationContract]
        void UpdateUserImage(int id, string picture);

        [OperationContract]
        void UpdateUserPassword(int id, string password);

        [OperationContract]
        User AddUser(User ev);

        [OperationContract]
        void DeleteUser(int id);

        [OperationContract]
        User ValidateUser(string userName, string password);

        [OperationContract]
        void AssignUserToEvent(int eventId, int userId);

        [OperationContract]
        IEnumerable<PlannerEntry> GetPlannerEntries(int userId);

        [OperationContract]
        PlannerEntry UpdatePlannerEntry(PlannerEntry entry);

        [OperationContract]
        PlannerEntry AddPlannerEntry(PlannerEntry entry);

    }
}
