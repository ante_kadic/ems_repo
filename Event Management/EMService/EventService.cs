﻿using Core.Common;
using EMS.Entities;
using EMS.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ServiceModel;

namespace Services
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class EventService : ServiceBase, IEventService
    {
        [Import]
        IRepositoryFactory _repositoryFactory;

        public EventService()
        {
        }

        public EventService(IRepositoryFactory repositoryFactory)
        {
            _repositoryFactory = repositoryFactory;
        }

        public Event GetEvent(int id)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                IEventRepository eventRepository = _repositoryFactory.GetRepository<IEventRepository>();

                Event ev = eventRepository.Get(id);
                if (ev == null)
                {
                    NotFoundException ex = new NotFoundException(string.Format($"Event with id:{id} not found in database"));
                    throw new FaultException<NotFoundException>(ex, ex.Message);
                }
                return ev;
            });
        }

        public IEnumerable<Event> GetEventsByUser(int userId)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                IEventRepository eventRepository = _repositoryFactory.GetRepository<IEventRepository>();
                return eventRepository.GetEventsByUser(userId);
            });
        }

        public IEnumerable<Event> GetAllEvents(int vendorId)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                IEventRepository eventRepository = _repositoryFactory.GetRepository<IEventRepository>();
                return eventRepository.GetAll(vendorId);
            });
        }

        public Event UpdateEvent(Event ev)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                Event updatedEvent;
                if (ev == null)
                {
                    ArgumentNullException ex = new ArgumentNullException("null event provided for update");
                    throw new FaultException<ArgumentNullException>(ex, ex.Message);
                }

                IEventRepository eventRepository = _repositoryFactory.GetRepository<IEventRepository>();

                if (ev.IDEvent == 0)
                    updatedEvent = eventRepository.Add(ev);
                else
                    updatedEvent = eventRepository.Update(ev);

                if (updatedEvent == null)
                {
                    NullReferenceException ex = new NullReferenceException(string.Format($"Event with ID of {ev.IDEvent} returned NULL after trying to update in database"));
                    throw new FaultException<NullReferenceException>(ex, ex.Message);
                }
                return updatedEvent;
            });
        }

        public Event AddEvent(Event ev)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                Event addedEvent;
                if (ev == null)
                {
                    ArgumentNullException ex = new ArgumentNullException("null event provided to add in database");
                    throw new FaultException<ArgumentNullException>(ex, ex.Message);
                }

                IEventRepository eventRepository = _repositoryFactory.GetRepository<IEventRepository>();
                addedEvent = eventRepository.Add(ev);
                if (addedEvent == null)
                {
                    NullReferenceException ex = new NullReferenceException(string.Format($"Event with ID of {ev.IDEvent} returned NULL after trying to add in database"));
                    throw new FaultException<NullReferenceException>(ex, ex.Message);
                }
                return addedEvent;
            });

        }

        public IEnumerable<WeddingHall> GetWeddingHalls(int vendorId)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                IEventRepository eventRepository = _repositoryFactory.GetRepository<IEventRepository>();
                return eventRepository.GetWeddingHalls(vendorId);
            });
        }

        public IEnumerable<WeddingHall> GetWeddingHallsEvents(int vendorId)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                IEventRepository eventRepository = _repositoryFactory.GetRepository<IEventRepository>();
                return eventRepository.GetWeddingHallsEvents(vendorId);
            });
        }

        public WeddingHall GetWeddingHall(int id)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                IEventRepository eventRepository = _repositoryFactory.GetRepository<IEventRepository>();
                return eventRepository.GetWeddingHall(id);
            });
        }

        public void DeleteEvent(int id)
        {
            throw new NotImplementedException();
        }

        public void UpdateEventDeposit(int eventId, decimal deposit, int currency)
        {
            ExecuteFaultHandledOperation(() =>
            {
                IEventRepository eventRepository = _repositoryFactory.GetRepository<IEventRepository>();
                eventRepository.UpdateEventDeposit(eventId, deposit, currency);
            });
        }

        public void UpdateEventState(int eventId, int newState)
        {
            ExecuteFaultHandledOperation(() =>
            {
                IEventRepository eventRepository = _repositoryFactory.GetRepository<IEventRepository>();
                 eventRepository.UpdateEventState(eventId, newState);
            });
        }

        public void UpdatePeopleNumber(int eventId, int peopleNo)
        {
            ExecuteFaultHandledOperation(() =>
            {
                IEventRepository eventRepository = _repositoryFactory.GetRepository<IEventRepository>();
                 eventRepository.UpdatePeopleNumber(eventId, peopleNo);
            });
        }

        public void UpdateEventMenu(int eventId, int menuId)
        {
            ExecuteFaultHandledOperation(() =>
            {
                IEventRepository eventRepository = _repositoryFactory.GetRepository<IEventRepository>();
                eventRepository.UpdateEventMenu(eventId, menuId);
            });
        }

        public IEnumerable<Menu> GetMenus(int vendorId)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                IEventRepository eventRepository = _repositoryFactory.GetRepository<IEventRepository>();
                return eventRepository.GetMenus(vendorId);
            });
        }

        public Menu AddMenu(Menu menu)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                IEventRepository eventRepository = _repositoryFactory.GetRepository<IEventRepository>();
                return eventRepository.AddMenu(menu);
            });
        }

        public Menu UpdateMenu(Menu menu)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                IEventRepository eventRepository = _repositoryFactory.GetRepository<IEventRepository>();
                return eventRepository.UpdateMenu(menu);
            });
        }

        public WeddingHall AddWeddingHall(WeddingHall weddingHall)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                IEventRepository eventRepository = _repositoryFactory.GetRepository<IEventRepository>();
                return eventRepository.AddWeddingHall(weddingHall);
            });
        }

        public WeddingHall UpdateWeddingHall(WeddingHall weddingHall)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                IEventRepository eventRepository = _repositoryFactory.GetRepository<IEventRepository>();
                return eventRepository.UpdateWeddingHall(weddingHall);
            });
        }
    }
}
