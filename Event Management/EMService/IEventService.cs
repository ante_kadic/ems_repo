﻿using Core.Common;
using EMS.Entities;
using Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Services
{
    [ServiceContract]
    public interface IEventService : IDataService
    {
        [OperationContract]
        Event GetEvent(int id);

        [OperationContract]
        IEnumerable<Event> GetAllEvents(int vendorId);

        [OperationContract]
        IEnumerable<Event> GetEventsByUser(int userId);

        [OperationContract]
        Event UpdateEvent(Event ev);

        [OperationContract]
        Event AddEvent(Event ev);

        [OperationContract]
        void DeleteEvent(int id);

        [OperationContract]
        IEnumerable<WeddingHall> GetWeddingHalls(int vendorId);

        [OperationContract]
        IEnumerable<WeddingHall> GetWeddingHallsEvents(int vendorId);

        [OperationContract]
        WeddingHall GetWeddingHall(int id);

        [OperationContract]
        void UpdateEventDeposit(int eventId, decimal deposit, int currency);

        [OperationContract]
        void UpdateEventState(int eventId, int newState);

        [OperationContract]
        void UpdatePeopleNumber(int eventId, int peopleNo);

        [OperationContract]
        void UpdateEventMenu(int eventId, int menuId);

        [OperationContract]
        IEnumerable<Menu> GetMenus(int vendorId);

        [OperationContract]
        Menu AddMenu(Menu menu);

        [OperationContract]
        Menu UpdateMenu(Menu menu);

        [OperationContract]
        WeddingHall AddWeddingHall(WeddingHall weddingHall);

        [OperationContract]
        WeddingHall UpdateWeddingHall(WeddingHall weddingHall);
    }


}
